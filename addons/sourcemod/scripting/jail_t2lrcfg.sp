#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <csgo_colors>
#include <emitsoundany>
#include <t2lrcfg>
#include <teamgames>
#include <sm_jail_redie>
#include <lastrequest>
#include <hlstatsX_adv>
//#pragma newdecls required
#define VERSION "1.1.22"

bool g_used, g_lr_avalible;
int g_iSeconds = -1;

char g_soundName[] = "sm_hosties/lr1.mp3";
Handle g_lrstart 				= null;
ConVar sv_deadtalk				= null;
ConVar sv_enablebunnyhopping	= null;
ConVar sv_autobunnyhopping	= null;

public Plugin myinfo = 
{
	name	= "[CS:GO]LastRequest Config",
	author	= "Darkeneez & ShaRen",
	version	= VERSION,
	url		= "http://www.vk.com/darkeneez97"
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_lastrequest", lr);
	RegConsoleCmd("sm_lr", lr);
	sv_deadtalk = FindConVar("sv_deadtalk");
	sv_enablebunnyhopping = FindConVar("sv_enablebunnyhopping");
	sv_autobunnyhopping = FindConVar("sv_autobunnyhopping");
	
	AddCommandListener(CallBack, "say_team");
	AddCommandListener(CallBack, "say");
	AddCommandListener(CallBack, "say2");
	
	HookEvent("round_start", round_start);
	HookEvent("round_end", Event_round_end, EventHookMode_Pre);
	g_lrstart = CreateGlobalForward("t2lrcfg_OnLrStarted", ET_Ignore, Param_Cell);
	CreateTimer(1.0, Message, _, TIMER_REPEAT);
}

public Action Message(Handle timer)
{
	if (!g_lr_avalible)
		return Plugin_Continue;
	bool bCT;
	for (int i=1; i<GetMaxClients(); i++) {
		if(IsValidCT(i) && !IsClientInLastRequest(i)) {
			bCT = true;
			continue;
		}
	}
	bool bT;
	for (int i=1; i<GetMaxClients(); i++)
		if (IsValidT(i) && !IsClientInLastRequest(i) && bCT) {
			bT = true;
			if (g_iSeconds == -1)			// если до этого не было кандидатов
				g_iSeconds = 20;
			if (g_iSeconds)
				PrintHintText(i, "У тебя %i сек, чтобы написать в чат !LR и играть в ЛР-игры", g_iSeconds);
			else PrintHintText(i, "Время истекло, вас могут убить, быстрее пиши !lr");
		}
	if (bT)	{
		for (int i = 1; i < GetMaxClients(); i++)
			if(IsValidCT(i) && !IsClientInLastRequest(i)) {
				if (g_iSeconds)
					PrintHintText(i, "У заключенных есть %i сек чтобы начать !lr", g_iSeconds);
				else PrintHintText(i, "Время истекло, заключенных можно убить за задержку раунда");
			}
		if (g_iSeconds)
			g_iSeconds--;
	} else g_iSeconds = -1;
	return Plugin_Continue;
}

public int OnAvailableLR() {
	g_lr_avalible = true;
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("IsLrActivated", Native_IsLrActivated);
	RegPluginLibrary("t2lrcfg");
	return APLRes_Success;
}

public int Native_IsLrActivated(Handle plugin, int numParams)
{
	return g_used ? true : false;
}

public Action Event_round_end(Event event, char[] name, bool silent)
{
	if (event.GetInt("winner") == 3)
		if (g_used)
			event.SetInt("reason", view_as<int>(CSRoundEnd_TerroristsNotEscaped)); 		// "Terrorists have not escaped"
		else event.SetInt("reason", view_as<int>(CSRoundEnd_TerroristsStopped));		// "Escaping terrorists have all been neutralized"
	else if (event.GetInt("winner") == 2) {
		if (g_used) {
			event.SetInt("winner", 3);
			CS_SetTeamScore(3, CS_GetTeamScore(3)+1);
			CS_SetTeamScore(2, CS_GetTeamScore(2)-1);
			PrintToChatAll("После ЛР победа всегда засчитывается в пользу КТ");
			event.SetInt("reason", view_as<int>(CSRoundEnd_CTStoppedEscape));			// "The CT's have prevented most of the terrorists from escaping"
		} else event.SetInt("reason", view_as<int>(CSRoundEnd_TerroristsEscaped));		// "Terrorists have escaped"
	}
	return Plugin_Continue;
}

public void round_start(Handle event, char[] name, bool silent)
{
	g_used = false;
	g_lr_avalible = false;
	
	ServerCommand("exec nolr.cfg");
	SetConVarBool(sv_deadtalk, false, _, true);
	SetConVarBool(sv_enablebunnyhopping, false, _, true);
	SetConVarBool(sv_autobunnyhopping, false, _, true);
}

public void OnMapStart()
{
	char sDownloadPath[256];

	PrecacheSoundAny(g_soundName, true);
	Format(sDownloadPath, PLATFORM_MAX_PATH, "sound/%s", g_soundName);
	AddFileToDownloadsTable(sDownloadPath);
}

public Action lr(int client, int args)
{
	if	(!CheckPlayers() || !IsValidT(client) || g_used)
		return Plugin_Handled;
	
	g_used = true;
	ServerCommand("exec lr.cfg");
	SetConVarBool(sv_deadtalk, true, _, true);
	SetConVarBool(sv_enablebunnyhopping, true, _, true);
	SetConVarBool(sv_autobunnyhopping, true, _, true);
	PrintHintTextToAll("<font color='#ff0000' size='30'>LastRequest Activated</font> Теперь живые слышат мертвых");
	TG_ClearTeam(TG_BlueTeam);
	TG_ClearTeam(TG_RedTeam);
	TG_StopGame(TG_BlueTeam);
	TG_StopGame(TG_RedTeam);
	EmitSoundToAllAny(g_soundName);
	
	Call_StartForward(g_lrstart);
	Call_Finish();
	for (int i=1; i<MAXPLAYERS; i++)
		if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 && GetClientPlayedTime(i) < 3600) {
			CGOPrintToChat(i, "{RED}ВНИМАНИЕ {GREEN}Сейчас началось время LR, не пытайтесь бунтовать");
			CGOPrintToChat(i, "{GREEN}Без причины убивать друг друга запрещено.");
			CGOPrintToChat(i, "{GREEN}понятие \"бунт\" отменяется до конца раунда");
		}
	return Plugin_Handled;
}

public Action CallBack(int client, char[] command, int args)
{
	char message[12];
	GetCmdArg(1, message, sizeof(message));
	
	if (StrEqual(message, "!LR") || StrEqual(message, "!дк") || StrEqual(message, "!ДК") || StrEqual(message, "!ir") || StrEqual(message, "!rl")
	|| StrEqual(message, "!лр") || StrEqual(message, "!ЛР") || StrEqual(message, " !lr") || StrEqual(message, " !LR")) //Если игрок пытается написать ЛР
		FakeClientCommand(client, "sm_lr");
}

bool CheckPlayers()
{
	int maxt;
	for (int i=1; i<=MaxClients; i++) {
		if (!IsValidT(i))
			continue;
		maxt++;
	}
	return (maxt > 2) ? false : true;
}

stock bool IsValidT(int client)
{
	return (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 2 || IsFakeClient(client)) ? false:true;
}

stock bool IsValidCT(int client)
{
	return (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 3 || IsFakeClient(client)) ? false:true;
}